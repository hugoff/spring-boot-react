const UrlRouter = {
  home: '/',
  comissao: {
    index: '/comissao',
    novo: '/comissao/novo',
    editar: '/comissao/editar/:id',
  },
  contrato: {
    index: '/contrato',
    novo: '/contrato/novo',
    editar: '/contrato/editar/:id',
  },
};

export default UrlRouter;
