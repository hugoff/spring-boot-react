import { PrimeIcons } from 'primereact/api';
import UrlRouter from './UrlRouter';

const menus = [
  { label: 'Dashboard', icon: PrimeIcons.HOME, to: '/' },
  { label: 'Comissão', icon: PrimeIcons.MONEY_BILL, to: UrlRouter.comissao.index },
  { label: 'Contrato', icon: PrimeIcons.PAPERCLIP, to: UrlRouter.contrato.index },
  { label: 'Crud Exemplo Layout', icon: PrimeIcons.USER_EDIT, to: '/crud' },
  {
    label: 'Hierarquia de Menu',
    icon: PrimeIcons.SEARCH,
    items: [
      {
        label: 'Submenu 1',
        icon: PrimeIcons.BOOKMARK,
        items: [
          {
            label: 'Submenu 1.1',
            icon: PrimeIcons.BOOKMARK,
            items: [
              { label: 'Submenu 1.1.1', icon: PrimeIcons.BOOKMARK },
              { label: 'Submenu 1.1.2', icon: PrimeIcons.BOOKMARK },
              { label: 'Submenu 1.1.3', icon: PrimeIcons.BOOKMARK },
            ],
          },
          {
            label: 'Submenu 1.2',
            icon: PrimeIcons.BOOKMARK,
            items: [
              { label: 'Submenu 1.2.1', icon: PrimeIcons.BOOKMARK },
              { label: 'Submenu 1.2.2', icon: PrimeIcons.BOOKMARK },
            ],
          },
        ],
      },
      {
        label: 'Submenu 2',
        icon: PrimeIcons.BOOKMARK,
        items: [
          {
            label: 'Submenu 2.1',
            icon: PrimeIcons.BOOKMARK,
            items: [
              { label: 'Submenu 2.1.1', icon: PrimeIcons.BOOKMARK },
              { label: 'Submenu 2.1.2', icon: PrimeIcons.BOOKMARK },
              { label: 'Submenu 2.1.3', icon: PrimeIcons.BOOKMARK },
            ],
          },
          {
            label: 'Submenu 2.2',
            icon: PrimeIcons.BOOKMARK,
            items: [
              { label: 'Submenu 2.2.1', icon: PrimeIcons.BOOKMARK },
              { label: 'Submenu 2.2.2', icon: PrimeIcons.BOOKMARK },
            ],
          },
        ],
      },
    ],
  },
];

export default menus;
