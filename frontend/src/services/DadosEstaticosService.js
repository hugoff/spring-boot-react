const DadosEstaticosService = {
  getTipoComissao() {
    return [
      { value: 'PERMANENTE', text: 'Permanente' },
      { value: 'ESPECIAL', text: 'Especial' },
    ];
  },
};

export default DadosEstaticosService;
