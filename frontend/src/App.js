import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import './layout/flags/flags.css';
import './layout/layout.scss';
import './App.scss';

import React from 'react';

import Template from './pages/Template';

import { Switch, Route } from 'react-router';
import { createBrowserHistory } from 'history';
import { Dashboard } from './components/Dashboard';
import Crud from './pages/Crud';

import PrimeReact from 'primereact/api';
import ComissaoIndexPage from './pages/showcase';
import UrlRouter from './constants/UrlRouter';
import EditComissao from './pages/showcase/edit';
import NewComissao from './pages/showcase/new';
import { BrowserRouter } from 'react-router-dom';
import ContratoIndexPage from './pages/contrato';
import EditContrato from './pages/contrato/edit';
import NewContrato from './pages/contrato/new';

PrimeReact.ripple = true;

require('dotenv').config();
const history = createBrowserHistory();

const App = () => {
  return (
    <BrowserRouter history={history}>
      <Template>
        <Switch>
          <>
            <Route path="/" exact component={Dashboard} />
            <Route path="/crud" exact component={Crud} />
            <Route path={UrlRouter.comissao.index} exact component={ComissaoIndexPage} />
            <Route path={UrlRouter.comissao.editar} exact component={EditComissao} />
            <Route path={UrlRouter.comissao.novo} exact component={NewComissao} />
            <Route path={UrlRouter.contrato.index} exact component={ContratoIndexPage} />
            <Route path={UrlRouter.contrato.editar} exact component={EditContrato} />
            <Route path={UrlRouter.contrato.novo} exact component={NewContrato} />
          </>
        </Switch>
      </Template>
    </BrowserRouter>
  );
};

export default App;
